# README #

This repo is for the work done for LNET paper 

It consists of building many machine learning models for UNSW dataset and Our own healthcare testbed dataset. 
The major contributions of the paper are in building a new evaluation metric for security application based on different applications requirements

Paper file include the paper directory

Datasets file include the dataset to be used for machine learning 

Simulation is for virtualizations for different metrics (Fig. 1 in the paper)

Core file is for ML codes 
